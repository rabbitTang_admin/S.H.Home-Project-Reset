package com.etang.shhomeproject.tools.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

/**
 * @ProjectName: S.H. Home Project
 * @Package: com.etang.shhomeproject.tools
 * @ClassName: MyDataBaseHelper
 * @Description: java类作用描述
 * @Author: 作者名：奶油话梅糖
 * @CreateDate: 2021/6/25 0025 2:03
 * @UpdateUser: 更新者：奶油话梅糖
 * @UpdateDate: 2021/6/25 0025 2:03
 * @UpdateRemark: 更新说明：
 */
public class MyDataBaseHelper extends SQLiteOpenHelper {
    /**
     * 构造方法
     *
     * @param context 继承context
     * @param name    数据库本地存储名字
     * @param factory
     * @param version
     */
    public MyDataBaseHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //创建数据库-用于检测APP打开状态
        db.execSQL("create table open_state (_id integer primary key autoincrement,open_state text)");
        //创建数据库-用于存储用户数据
        db.execSQL("create table user (_id integer primary key autoincrement,username text,passward text)");
        //向数据库内插入默认数据
        db.execSQL("insert into open_state (open_state)values(?)", new String[]{"false"});
        db.execSQL("insert into user (username,passward)values(?,?)", new String[]{"admin", "123456"});
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
