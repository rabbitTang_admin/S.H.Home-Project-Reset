package com.etang.shhomeproject.tools.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.etang.shhomeproject.R;

/**
 * 显示bug信息的dialog
 */
public class DialogDebugs {
    /**
     * 显示dialog的方法
     *
     * @param activity 继承activity
     * @param message  显示dialog的信息
     * @param error    显示dialog的错误信息
     */
    public static void showdialog(Activity activity, String message, String error) {
        //新建view
        View view = LayoutInflater.from(activity).inflate(R.layout.dialog_error, null, false);
        //新建alerdialog，继承自activity
        AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
        //给alerdialog设置view
        alertDialog.setView(view);
        //设置关闭点击弹出框外部关闭弹出框功能
        alertDialog.setCanceledOnTouchOutside(false);
        //按钮2
        final Button btn_dialog_nomo_2 = (Button) view.findViewById(R.id.btn_dialog_debug_2);
        //dialog的标题
        final TextView tv_dialog_nomo_title = (TextView) view.findViewById(R.id.tv_dialog_debug_title);
        //dialog的信息
        final TextView tv_dialog_nomo_message = (TextView) view.findViewById(R.id.tv_dialog_debug_message);
        //设置标题
        tv_dialog_nomo_title.setText(activity.getString(R.string.dialog_info_title_error));
        //设置信息
        tv_dialog_nomo_message.setText(message + "\n" + error);
        //设置按钮1文本
        btn_dialog_nomo_2.setText(activity.getString(R.string.button_cls));
        //设置按钮1点击事件
        btn_dialog_nomo_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //关闭alerdialog
                alertDialog.dismiss();
            }
        });
        //显示alerdialog
        alertDialog.show();
    }
}
