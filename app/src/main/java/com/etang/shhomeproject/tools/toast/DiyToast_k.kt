package com.etang.shhomeproject.tools.toast

import android.content.Context
import android.widget.Toast

/**
 * @ProjectName: S.H. Home Project
 * @Package: com.etang.shhomeproject.tools
 * @ClassName: DiyToast
 * @Description: java类作用描述
 * @Author: 作者名：奶油话梅糖
 * @CreateDate: 2021/6/24 0024 19:33
 * @UpdateUser: 更新者：奶油话梅糖
 * @UpdateDate: 2021/6/24 0024 19:33
 * @UpdateRemark: 更新说明：
 */
object DiyToast_k {
    //新建toast
    private var toast: Toast? = null

    /**
     * 新建一个展示toast的方法
     *
     * @param context 继承context
     * @param s       要显示的内容
     */
    @JvmStatic
    fun showToast(context: Context?, s: String?) {
        //如果toast未被创建
        if (toast == null) {
            //创建一个toast
            toast = Toast.makeText(context, s, Toast.LENGTH_LONG)
        } else {
            //设置toast文本
            toast!!.setText(s)
        }
        //设置toast位置
//        toast!!.setGravity(Gravity.CENTER, 0, 0)
        //显示toast
        toast!!.show()
    }
}