package com.etang.shhomeproject.appstart.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.etang.shhomeproject.R;
import com.etang.shhomeproject.tools.AppConfig;
import com.etang.shhomeproject.tools.dialog.CheckUpdateDialog;
import com.etang.shhomeproject.tools.view.CodeView;
import com.etang.shhomeproject.tools.toast.DiyToast_k;
import com.etang.shhomeproject.tools.dialog.DialogOpenS;

import java.util.ArrayList;

/**
 * "我的"界面 （半成品）
 */
public class MeFragment extends Fragment {
    private TextView tv_me_osxieyi;
    private Button btn_me_checkupdate, btn_bord_data_id_con, btn_bord_control_id_con;
    private EditText et_bordid_temp, et_bordid_smo, et_bordid_gas, et_bordid_press, et_bordid_ill,
            et_bordid_co, et_bordid_pm, et_bordid_lamp, et_bordid_warm, et_bordid_cur,
            et_bordid_inf, et_bordid_door, et_bordid_fan, et_bordid_per;
    private ArrayList<String> arrayList = new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        if (AppConfig.EINK_MODE == true) {
            //Eink屏幕
            //设置View
            view = inflater.inflate(R.layout.fragment_my_eink, null, false);
        } else {
            Log.i("eink_mode_checker", "当前不是Eink模式");
            //设置View
            view = inflater.inflate(R.layout.fragment_my, null, false);
        }
        AppConfig.getBORDID(getActivity());
        initView(view);
        initBORDID();
        tv_me_osxieyi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogOpenS.showToast(getActivity(), getActivity());
            }
        });
        btn_me_checkupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckUpdateDialog.check_update(getActivity(), getActivity(), "about");
            }
        });
        btn_bord_control_id_con.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_bordid_co.getText().toString().isEmpty() ||
                        et_bordid_cur.getText().toString().isEmpty() ||
                        et_bordid_door.getText().toString().isEmpty() ||
                        et_bordid_fan.getText().toString().isEmpty() ||
                        et_bordid_gas.getText().toString().isEmpty() ||
                        et_bordid_ill.getText().toString().isEmpty() ||
                        et_bordid_inf.getText().toString().isEmpty() ||
                        et_bordid_lamp.getText().toString().isEmpty() ||
                        et_bordid_pm.getText().toString().isEmpty() ||
                        et_bordid_press.getText().toString().isEmpty() ||
                        et_bordid_smo.getText().toString().isEmpty() ||
                        et_bordid_temp.getText().toString().isEmpty() ||
                        et_bordid_warm.getText().toString().isEmpty()) {
                    DiyToast_k.showToast(getActivity(), "不能有空白项");
                } else {
                    if (arrayList.size() != 0) {
                        arrayList.clear();
                        /**
                         * 顺序一般是：
                         * 1.温度\湿度
                         * 2.烟雾
                         * 3.燃气
                         * 4.气压
                         * 5.人体红外
                         * 6.Co2
                         * 7.PM25
                         * 8.光照
                         * 9.射灯
                         * 10.风扇
                         * 11.报警灯
                         * 12.门禁
                         * 13.窗帘
                         * 14.红外转发
                         */
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                arrayList.add(et_bordid_temp.getText().toString());
                                arrayList.add(et_bordid_smo.getText().toString());
                                arrayList.add(et_bordid_gas.getText().toString());
                                arrayList.add(et_bordid_press.getText().toString());
                                arrayList.add(et_bordid_per.getText().toString());
                                arrayList.add(et_bordid_co.getText().toString());
                                arrayList.add(et_bordid_pm.getText().toString());
                                arrayList.add(et_bordid_ill.getText().toString());
                                arrayList.add(et_bordid_lamp.getText().toString());
                                arrayList.add(et_bordid_fan.getText().toString());
                                arrayList.add(et_bordid_warm.getText().toString());
                                arrayList.add(et_bordid_door.getText().toString());
                                arrayList.add(et_bordid_cur.getText().toString());
                                arrayList.add(et_bordid_inf.getText().toString());
                                AppConfig.setBORDID(getActivity(), getActivity(), arrayList);
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        initBORDID();
                                    }
                                });
                            }
                        }).start();
                    } else {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                arrayList.add(et_bordid_temp.getText().toString());
                                arrayList.add(et_bordid_smo.getText().toString());
                                arrayList.add(et_bordid_gas.getText().toString());
                                arrayList.add(et_bordid_press.getText().toString());
                                arrayList.add(et_bordid_per.getText().toString());
                                arrayList.add(et_bordid_co.getText().toString());
                                arrayList.add(et_bordid_pm.getText().toString());
                                arrayList.add(et_bordid_ill.getText().toString());
                                arrayList.add(et_bordid_lamp.getText().toString());
                                arrayList.add(et_bordid_fan.getText().toString());
                                arrayList.add(et_bordid_warm.getText().toString());
                                arrayList.add(et_bordid_door.getText().toString());
                                arrayList.add(et_bordid_cur.getText().toString());
                                arrayList.add(et_bordid_inf.getText().toString());
                                AppConfig.setBORDID(getActivity(), getActivity(), arrayList);
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        initBORDID();
                                    }
                                });
                            }
                        }).start();
                    }
                    DiyToast_k.showToast(getActivity(), "请重启APP");
                }
            }
        });
        btn_bord_data_id_con.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_bordid_co.getText().toString().isEmpty() ||
                        et_bordid_cur.getText().toString().isEmpty() ||
                        et_bordid_door.getText().toString().isEmpty() ||
                        et_bordid_fan.getText().toString().isEmpty() ||
                        et_bordid_gas.getText().toString().isEmpty() ||
                        et_bordid_ill.getText().toString().isEmpty() ||
                        et_bordid_inf.getText().toString().isEmpty() ||
                        et_bordid_lamp.getText().toString().isEmpty() ||
                        et_bordid_pm.getText().toString().isEmpty() ||
                        et_bordid_press.getText().toString().isEmpty() ||
                        et_bordid_smo.getText().toString().isEmpty() ||
                        et_bordid_temp.getText().toString().isEmpty() ||
                        et_bordid_warm.getText().toString().isEmpty()) {
                    DiyToast_k.showToast(getActivity(), "不能有空白项");
                } else {
                    if (arrayList.size() != 0) {
                        arrayList.clear();
                        /**
                         * 顺序一般是：
                         * 1.温度\湿度
                         * 2.烟雾
                         * 3.燃气
                         * 4.气压
                         * 5.人体红外
                         * 6.Co2
                         * 7.PM25
                         * 8.光照
                         * 9.射灯
                         * 10.风扇
                         * 11.报警灯
                         * 12.门禁
                         * 13.窗帘
                         * 14.红外转发
                         */
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                arrayList.add(et_bordid_temp.getText().toString());
                                arrayList.add(et_bordid_smo.getText().toString());
                                arrayList.add(et_bordid_gas.getText().toString());
                                arrayList.add(et_bordid_press.getText().toString());
                                arrayList.add(et_bordid_per.getText().toString());
                                arrayList.add(et_bordid_co.getText().toString());
                                arrayList.add(et_bordid_pm.getText().toString());
                                arrayList.add(et_bordid_ill.getText().toString());
                                arrayList.add(et_bordid_lamp.getText().toString());
                                arrayList.add(et_bordid_fan.getText().toString());
                                arrayList.add(et_bordid_warm.getText().toString());
                                arrayList.add(et_bordid_door.getText().toString());
                                arrayList.add(et_bordid_cur.getText().toString());
                                arrayList.add(et_bordid_inf.getText().toString());
                                AppConfig.setBORDID(getActivity(), getActivity(), arrayList);
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        initBORDID();
                                    }
                                });
                            }
                        }).start();
                    } else {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                arrayList.add(et_bordid_temp.getText().toString());
                                arrayList.add(et_bordid_smo.getText().toString());
                                arrayList.add(et_bordid_gas.getText().toString());
                                arrayList.add(et_bordid_press.getText().toString());
                                arrayList.add(et_bordid_per.getText().toString());
                                arrayList.add(et_bordid_co.getText().toString());
                                arrayList.add(et_bordid_pm.getText().toString());
                                arrayList.add(et_bordid_ill.getText().toString());
                                arrayList.add(et_bordid_lamp.getText().toString());
                                arrayList.add(et_bordid_fan.getText().toString());
                                arrayList.add(et_bordid_warm.getText().toString());
                                arrayList.add(et_bordid_door.getText().toString());
                                arrayList.add(et_bordid_cur.getText().toString());
                                arrayList.add(et_bordid_inf.getText().toString());
                                AppConfig.setBORDID(getActivity(), getActivity(), arrayList);
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        initBORDID();
                                    }
                                });
                            }
                        }).start();
                    }
                }
                DiyToast_k.showToast(getActivity(), "请重启APP");
            }
        });
        //设置语言
//        setLanguage();
        return view;
    }

    private void initBORDID() {
        /**
         * 顺序一般是：
         * 1.温度\湿度
         * 2.烟雾
         * 3.燃气
         * 4.气压
         * 5.人体红外
         * 6.Co2
         * 7.PM25
         * 8.光照
         * 9.射灯
         * 10.风扇
         * 11.报警灯
         * 12.门禁
         * 13.窗帘
         * 14.红外转发
         */
        arrayList.add(AppConfig.BORDID_TEMP_HUM);
        arrayList.add(AppConfig.BORDID_SMO);
        arrayList.add(AppConfig.BORDID_GAS);
        arrayList.add(AppConfig.BORDID_PRESS);
        arrayList.add(AppConfig.BORDID_PER);
        arrayList.add(AppConfig.BORDID_CO);
        arrayList.add(AppConfig.BORDID_PM);
        arrayList.add(AppConfig.BORDID_ILL);
        arrayList.add(AppConfig.BORDID_LAMP);
        arrayList.add(AppConfig.BORDID_FAN);
        arrayList.add(AppConfig.BORDID_WARM);
        arrayList.add(AppConfig.BORDID_DOOR);
        arrayList.add(AppConfig.BORDID_CUR);
        arrayList.add(AppConfig.BORDID_INF);
        //設置默認數值
        et_bordid_warm.setText(AppConfig.BORDID_WARM);
        et_bordid_temp.setText(AppConfig.BORDID_TEMP_HUM);
        et_bordid_smo.setText(AppConfig.BORDID_SMO);
        et_bordid_press.setText(AppConfig.BORDID_PRESS);
        et_bordid_pm.setText(AppConfig.BORDID_PM);
        et_bordid_per.setText(AppConfig.BORDID_PER);
        et_bordid_lamp.setText(AppConfig.BORDID_LAMP);
        et_bordid_inf.setText(AppConfig.BORDID_INF);
        et_bordid_ill.setText(AppConfig.BORDID_ILL);
        et_bordid_gas.setText(AppConfig.BORDID_GAS);
        et_bordid_fan.setText(AppConfig.BORDID_FAN);
        et_bordid_door.setText(AppConfig.BORDID_DOOR);
        et_bordid_cur.setText(AppConfig.BORDID_CUR);
        et_bordid_co.setText(AppConfig.BORDID_CO);
    }

    private void initView(View view) {
        et_bordid_co = (EditText) view.findViewById(R.id.et_bordid_co);
        et_bordid_cur = (EditText) view.findViewById(R.id.et_bordid_cur);
        et_bordid_door = (EditText) view.findViewById(R.id.et_bordid_door);
        et_bordid_fan = (EditText) view.findViewById(R.id.et_bordid_fan);
        et_bordid_gas = (EditText) view.findViewById(R.id.et_bordid_gas);
        et_bordid_ill = (EditText) view.findViewById(R.id.et_bordid_ill);
        et_bordid_inf = (EditText) view.findViewById(R.id.et_bordid_redsend);
        et_bordid_lamp = (EditText) view.findViewById(R.id.et_bordid_lamp);
        et_bordid_pm = (EditText) view.findViewById(R.id.et_bordid_pm);
        et_bordid_press = (EditText) view.findViewById(R.id.et_bordid_press);
        et_bordid_smo = (EditText) view.findViewById(R.id.et_bordid_smo);
        et_bordid_temp = (EditText) view.findViewById(R.id.et_bordid_temp);
        et_bordid_warm = (EditText) view.findViewById(R.id.et_bordid_warm);
        et_bordid_per = (EditText) view.findViewById(R.id.et_bordid_per);
        btn_bord_control_id_con = (Button) view.findViewById(R.id.btn_bord_control_id_con);
        btn_bord_data_id_con = (Button) view.findViewById(R.id.btn_bord_data_id_con);
        btn_me_checkupdate = (Button) view.findViewById(R.id.btn_me_checkupdate);
        tv_me_osxieyi = (TextView) view.findViewById(R.id.tv_me_osxieyi);
    }
}
