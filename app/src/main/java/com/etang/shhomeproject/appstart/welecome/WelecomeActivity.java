package com.etang.shhomeproject.appstart.welecome;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.etang.shhomeproject.appstart.MainActivity;
import com.etang.shhomeproject.R;
import com.etang.shhomeproject.tools.sql.MyDataBaseHelper;
import com.etang.shhomeproject.tools.permission.SavePermission;

public class WelecomeActivity extends AppCompatActivity {
    private Button btn_welecome_quanxian;
    private static final int BAIDU_READ_PHONE_STATE = 100;
    private MyDataBaseHelper dbHelper;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welecome);
        initView();
        btn_welecome_quanxian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check_permission_void();
            }
        });
    }

    private void initView() {
        btn_welecome_quanxian = (Button) findViewById(R.id.btn_welecome_quanxian);
        dbHelper = new MyDataBaseHelper(this, "info.db", null, 2);
        db = dbHelper.getWritableDatabase();
    }

    /**
     * 检查存储权限
     */
    private void check_permission_void() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (!SavePermission.check_save_permission(WelecomeActivity.this)) {
                    while (!SavePermission.check_save_permission(WelecomeActivity.this)) {
                        try {
                            Thread.sleep(500);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (SavePermission.check_save_permission(WelecomeActivity.this)) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (SavePermission.check_save_permission(WelecomeActivity.this)) {
                                        db.execSQL("update open_state set open_state = ?", new String[]{"true"});
                                        startActivity(new Intent(WelecomeActivity.this, MainActivity.class));
                                        finish();
                                    } else {
                                        System.exit(0);
                                    }
                                }
                            });
                        }
                    }
                } else {
                    if (SavePermission.check_save_permission(WelecomeActivity.this)) {
                        db.execSQL("update open_state set open_state = ?", new String[]{"true"});
                        startActivity(new Intent(WelecomeActivity.this, MainActivity.class));
                        finish();
                    } else {
                        System.exit(0);
                    }
                }
            }
        }).start();
    }


}