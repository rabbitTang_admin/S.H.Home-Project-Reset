package com.etang.shhomeproject.appstart

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.widget.SeekBar
import com.etang.shhomeproject.tools.sql.MyDataBaseHelper
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.content.Intent
import android.content.res.Configuration
import android.os.Handler
import android.os.Message
import android.util.Log
import android.view.View
import com.etang.shhomeproject.appstart.welecome.WelecomeActivity
import com.etang.shhomeproject.appstart.loginandregist.LoginActivity
import com.etang.shhomeproject.R
import com.etang.shhomeproject.tools.AppConfig
import com.etang.shhomeproject.tools.dialog.DeBugDialog

class MainActivity : AppCompatActivity() {
    //新建SeekBar
    private var sk_loading: SeekBar? = null

    //新建一个int类型函数
    private var number = 0

    //数据库
    private var dbHelper: MyDataBaseHelper? = null
    private var db: SQLiteDatabase? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getOrientation(this@MainActivity)
        //从AppConfig检查Eink模式
        AppConfig.checkEink(this)
        //检查Eink
        initEink()
        //绑定控件
        initView()
        //激活异常捕获
//        CrashHandler.getInstance().init(this);
        //检测APP是否为第一次打开
        check_first_open()
    }

    private fun initEink() {
        if (AppConfig.EINK_MODE) {
            //Eink屏幕
            setContentView(R.layout.activity_main_portrait_eink)
        } else {
            Log.i("eink_mode_checker", "当前不是Eink模式")
            getOrientation(this)
        }
    }

    private fun check_first_open() {
        val cursor = db!!.rawQuery("select * from open_state", null)
        cursor.moveToFirst()
        val open_state = cursor.getString(cursor.getColumnIndex("open_state"))
        if (open_state == "false") {
            startActivity(Intent(this@MainActivity, WelecomeActivity::class.java))
            finish()
        } else {
            //启动handler线程
            handler.post(timeRunnable)
        }
    }

    /**
     * 进度条加载线程
     * 新建handler线程
     */
    var handler: Handler = object : Handler() {
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            //设置seekbar进度
            sk_loading!!.progress = number
            //设置handler线程延迟
            this.postDelayed(timeRunnable, 10)
        }
    }

    //新建Runnable线程
    var timeRunnable: Runnable = object : Runnable {
        override fun run() {
            //让int自增加
            number++
            //新建消息给handler更新
            val msg = handler.obtainMessage()
            //为上面的handler线程留出判断值的空间、时间
            if (number > 101) {
                //移除线程
                handler.removeCallbacks(this)
                //跳转到登录界面
                startActivity(Intent(this@MainActivity, LoginActivity::class.java))
                //结束当前Activity
                finish()
            } else {
                //给handler线程发送信息，让handler线程更新
                handler.sendMessage(msg)
            }
        }
    }

    /**
     * 绑定控件
     */
    private fun initView() {
        //SeekBar滑动栏，用于显示进度
        sk_loading = findViewById<View>(R.id.sk_loading) as SeekBar
        //数据库
        dbHelper = MyDataBaseHelper(this@MainActivity, "info.db", null, 2)
        db = dbHelper!!.writableDatabase
    }

    private fun getOrientation(ctx: Context) {
        if (ctx.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            //竖屏
            setContentView(R.layout.activity_main_portrait)
        } else {
            //横屏
            setContentView(R.layout.activity_main_landscape)
        }
    }

    /* *
     * 屏幕旋转时调用此方法
     * */
    override fun onConfigurationChanged(newConfig: Configuration) {
        getOrientation(this@MainActivity)
        super.onConfigurationChanged(newConfig)
    }
}