package com.etang.shhomeproject.appstart.fragment;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.bizideal.smarthome.socket.Utils.SpUtils;
import com.etang.shhomeproject.R;
import com.etang.shhomeproject.tools.AppConfig;

import java.util.Locale;

/**
 * 数据获取界面
 */
public class BaseFragment extends Fragment {
    //人体红外图标
    private ImageView iv_per_state;
    //文本：人体红外、烟雾、燃气、气压、PM2.5、Co2、温度、光照、湿度
    private TextView tv_per, tv_smo, tv_gas, tv_press, tv_pm, tv_co, tv_temp, tv_ill, tv_hum;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        if (AppConfig.EINK_MODE == true) {
            //Eink屏幕
            //设置View
            view = inflater.inflate(R.layout.fragment_base_eink, null, false);
        } else {
            Log.i("eink_mode_checker", "当前不是Eink模式");
            //设置View
            view = inflater.inflate(R.layout.fragment_base, null, false);
        }
        //绑定控件
        initView(view);
        //开启线程
        handler.post(timeRunna);
        return view;
    }

    /**
     * 给文本设置文本的线程
     */
    Handler handler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            //如果per的值==1，则是有人，否则没人
            if (AppConfig.per == 1) {
                iv_per_state.setImageResource(R.drawable.ic_per_off);
                tv_per.setText(getString(R.string.base_noman));
            } else {
                iv_per_state.setImageResource(R.drawable.ic_per_on);
                tv_per.setText(getString(R.string.base_hasman));
            }
            //CO2
            tv_co.setText(AppConfig.co + "  ppm");
            //燃气
            tv_gas.setText(AppConfig.gas + "  m³");
            //湿度
            tv_hum.setText(AppConfig.hum + "  %");
            //光照
            tv_ill.setText(AppConfig.ill + "  Lux");
            //PM2.5
            tv_pm.setText(AppConfig.pm + "  μg/m3");
            //气压
            tv_press.setText(AppConfig.press + "  Pa");
            //烟雾
            tv_smo.setText(AppConfig.smo + "  ppm");
            //温度
            tv_temp.setText(AppConfig.temp + "  ℃");
            //设置线程运行间隔
            handler.postDelayed(timeRunna, 500);
        }
    };
    /**
     * Runnable辅助线程
     */
    Runnable timeRunna = new Runnable() {
        @Override
        public void run() {
            //接受消息
            Message msg = handler.obtainMessage();
            //发送消息
            handler.sendMessage(msg);
        }
    };

    /**
     * 绑定控件
     *
     * @param view 继承View
     */
    private void initView(View view) {
        iv_per_state = (ImageView) view.findViewById(R.id.iv_per_state);
        tv_co = (TextView) view.findViewById(R.id.tv_co);
        tv_gas = (TextView) view.findViewById(R.id.tv_gas);
        tv_hum = (TextView) view.findViewById(R.id.tv_hum);
        tv_ill = (TextView) view.findViewById(R.id.tv_ill);
        tv_per = (TextView) view.findViewById(R.id.tv_per);
        tv_pm = (TextView) view.findViewById(R.id.tv_pm);
        tv_press = (TextView) view.findViewById(R.id.tv_press);
        tv_smo = (TextView) view.findViewById(R.id.tv_smo);
        tv_temp = (TextView) view.findViewById(R.id.tv_temp);
    }
}
