package com.etang.shhomeproject.appstart.loginandregist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bizideal.smarthome.socket.SocketClient;
import com.etang.shhomeproject.R;
import com.etang.shhomeproject.appstart.MainActivity;
import com.etang.shhomeproject.appstart.fragment.BarActivity;
import com.etang.shhomeproject.tools.AppConfig;
import com.etang.shhomeproject.tools.dialog.CheckUpdateDialog;
import com.etang.shhomeproject.tools.dialog.DeBugDialog;
import com.etang.shhomeproject.tools.toast.DiyToast_k;
import com.etang.shhomeproject.tools.sql.MyDataBaseHelper;

public class LoginActivity extends AppCompatActivity {

    private Button btn_login;
    private EditText et_login_username, et_login_passward, et_login_ip, et_login_codepass;
    private String username, passward, ip, codepass;
    private ImageView iv_code_show;
    private MyDataBaseHelper dbHelper;
    private SQLiteDatabase db;
    private SharedPreferences sharedPreferences;
    private TextView tv_login_onclicktoeink;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getOrientation(LoginActivity.this);
        sharedPreferences = getSharedPreferences("rember", MODE_PRIVATE);
        //检查当前是否为Eink模式
        initEink();
        //绑定控件
        initView();
        //检查记住密码
        initRemb();
        //检查更新
        CheckUpdateDialog.check_update(LoginActivity.this, LoginActivity.this, "login");
        /**
         * 暂停使用验证码
         */
//        //给imageview设置bitmap
//        iv_code_show.setImageBitmap(AppConfig.createBitmap());
//        //设置iv的点击事件
//        iv_code_show.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //给imageview设置bitmap
//                iv_code_show.setImageBitmap(AppConfig.createBitmap());
//            }
//        });
        tv_login_onclicktoeink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.getBoolean("eink_mode", false) == true) {
                    sharedPreferences.edit().putBoolean("eink_mode", false).commit();
                    getOrientation(LoginActivity.this);
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();
                } else {
                    sharedPreferences.edit().putBoolean("eink_mode", true).commit();
                    initEink();
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();
                }
            }
        });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_login_ip.getText().toString().isEmpty()
                        || et_login_passward.getText().toString().isEmpty()
                        || et_login_username.getText().toString().isEmpty()) {
                    DiyToast_k.showToast(LoginActivity.this, "不能有空白项");
                } else {
                    Cursor cursor = db.rawQuery("select * from user where username = ? and passward = ?",
                            new String[]{et_login_username.getText().toString(), et_login_passward.getText().toString()});
                    if (cursor.moveToNext()) {
                        SocketClient.ip = et_login_ip.getText().toString();
                        sharedPreferences.edit().putString("user", et_login_username.getText().toString())
                                .putString("pass", et_login_passward.getText().toString())
                                .putString("ip", et_login_ip.getText().toString()).commit();
                        startActivity(new Intent(LoginActivity.this, BarActivity.class));
                    } else {
                        DiyToast_k.showToast(LoginActivity.this, "用户名或密码输入错误");
                    }
                }
            }
        });
    }

    /**
     * 绑定控件
     */
    private void initView() {
        iv_code_show = (ImageView) findViewById(R.id.iv_code_show);
        tv_login_onclicktoeink = (TextView) findViewById(R.id.tv_login_onclicktoeink);
        btn_login = (Button) findViewById(R.id.btn_login);
        et_login_ip = (EditText) findViewById(R.id.et_login_ip);
        et_login_codepass = (EditText) findViewById(R.id.et_login_codepass);
        et_login_passward = (EditText) findViewById(R.id.et_login_passward);
        et_login_username = (EditText) findViewById(R.id.et_login_username);
        dbHelper = new MyDataBaseHelper(this, "info.db", null, 2);
        db = dbHelper.getWritableDatabase();
    }

    private void initEink() {
        if (sharedPreferences.getBoolean("eink_mode", false) == true) {
            //Eink屏幕
            setContentView(R.layout.activity_login_eink);
            AppConfig.EINK_MODE = true;
        } else {
            Log.i("eink_mode_checker", "当前不是Eink模式");
            getOrientation(LoginActivity.this);
            AppConfig.EINK_MODE = false;
        }
    }

    /**
     * 检查记住密码
     */
    private void initRemb() {
        try {
            et_login_username.setText(sharedPreferences.getString("user", null));
            et_login_passward.setText(sharedPreferences.getString("pass", null));
            et_login_ip.setText(sharedPreferences.getString("ip", null));
        } catch (Exception e) {

        }
    }

    /**
     * 退出程序按钮的映射方法
     *
     * @param view
     */
//    public static void login_exit(View view) {
//        //退出程序
//        System.exit(0);
//    }

    /**
     * 注册账号按钮的映射方法
     *
     * @param view
     */
    public static void reg(View view) {
        //打开注册界面
        view.getContext().startActivity(new Intent(view.getContext(), RegActivity.class));
    }

    private void getOrientation(Context ctx) {
        if (ctx.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            //竖屏
            setContentView(R.layout.activity_login_portrait);
        } else {
            //横屏
            setContentView(R.layout.activity_login_landscape);
        }
    }

    /* *
     * 屏幕旋转时调用此方法
     * */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (sharedPreferences.getBoolean("eink_mode", false) == true) {
            DiyToast_k.showToast(LoginActivity.this, "Eink模式不支持横竖屏切换！");
        } else {
            getOrientation(LoginActivity.this);
        }
        super.onConfigurationChanged(newConfig);
    }
}