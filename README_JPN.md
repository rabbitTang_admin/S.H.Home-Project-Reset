## S.H. Home Resert Project

> 基于企想智能家居安装与维护平台开发的APP

## 截图

| 登录界面                                                     | 数据采集界面                                                 | 设备控制界面（打开）                                         | 设备控制界面（关闭）                                         | 软件设置界面                                                 | 加载界面                                                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![](https://rabbittang_admin.gitee.io/gallery/Screenshots/Screenshot_2021-08-07-01-47-52-448_S.H.%20Home.png) | ![](https://rabbittang_admin.gitee.io/gallery/Screenshots/Screenshot_2021-08-07-01-48-20-798_S.H.%20Home.png) | ![](https://rabbittang_admin.gitee.io/gallery/Screenshots/Screenshot_2021-08-07-01-48-53-907_S.H.%20Home.png) | ![](https://rabbittang_admin.gitee.io/gallery/Screenshots/Screenshot_2021-08-07-01-48-34-612_S.H.%20Home.png) | ![](https://rabbittang_admin.gitee.io/gallery/Screenshots/Screenshot_2021-08-07-01-49-19-720_S.H.%20Home.png) | ![](https://rabbittang_admin.gitee.io/gallery/Screenshots/Screenshot_2021-08-07-01-52-36-569_S.H.%20Home.png) |

## 前言

> 请根据您的语言点击相对应的连接来查看不同语言的README.md
>
> Please click the corresponding link according to your language to view the README.md in different languages.

| 语言    | 版本 | 链接 |
| ------- | ---- | ---- |
| English | 暂无 | 暂无 |
| 中文    | 1.0  | 当前 |
| 日本語  | 暂无 | 暂无 |

#### 更新地址

- 最新更新在：[Gitee码云](https://gitee.com/rabbitTang_admin/S.H.Home-Project-Reset)
- 我的博客是：[どうして私だけが](https://blog.nyanon.online/)

## 介绍

### 关于开源

本项目已与``2019年12月01日``开源，采用**[Apache License 2.0](https://gitee.com/rabbitTang_admin/NT-Eink-Launcher/blob/master/LICENSE)**许可证，各取所需。

### 关于跨平台

~~本项目已通过`Kotlin`混合编程实现跨平台，理论支持Mac、Linux、Windows等平台~~

暂时放弃跨平台（很多API需要重写才能实现功能，日后有空再维护）

### 用户文档

编写中

## 赞赏

| 微信                                                         | 支付宝                                                       |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![微信](https://rabbittang_admin.gitee.io/gallery/pay_core/wechatpay.jpg) | ![支付宝](https://rabbittang_admin.gitee.io/gallery/pay_core/alipay.jpg) |



